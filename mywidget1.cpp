#include "mywidget1.h"

MyWidget1::MyWidget1(QWidget *parent)
    : QWidget(parent)
{
    button.setParent(this);
    button.setText("CLOSE");
    button.move(200,200);
    connect(&button,&QPushButton::pressed,this,&MyWidget1::close);
}

MyWidget1::~MyWidget1()
{

}
