#ifndef MYWIDGET1_H
#define MYWIDGET1_H

#include <QWidget>
#include<QPushButton>

class MyWidget1 : public QWidget
{
    Q_OBJECT

public:
    MyWidget1(QWidget *parent = 0);
    ~MyWidget1();
private:
    QPushButton button;
};

#endif // MYWIDGET1_H
